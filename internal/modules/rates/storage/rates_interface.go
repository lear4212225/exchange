package storage

import (
	"context"

	"gitlab.com/ptflp/go-rpc/internal/models"
)

type Rater interface {
	AddTicker(ctx context.Context, ticker models.RatesDTO) error
	GetMinPrices(ctx context.Context) []models.MinRatesDTO
	GetMaxPrices(ctx context.Context) []models.MaxRatesDTO
	CreateMinPrice(ctx context.Context, symbol string, price float64) error
	CreateMaxPrice(ctx context.Context, symbol string, price float64) error
	UpdateMinPrice(ctx context.Context, symbol string, price float64) error
	UpdateMaxPrice(ctx context.Context, symbol string, price float64) error
	GetLastTicker(ctx context.Context) (string, error)
}
