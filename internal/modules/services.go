package modules

import (
	"gitlab.com/ptflp/go-rpc/internal/infrastructure/component"
	eservice "gitlab.com/ptflp/go-rpc/internal/infrastructure/service"
	"gitlab.com/ptflp/go-rpc/internal/modules/rates/service"
	"gitlab.com/ptflp/go-rpc/internal/storages"
)

type Services struct {
	Exchange eservice.Exchenger
	Rates    service.RatesServer
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	return &Services{
		Exchange: eservice.NewExchange(components.Logger),
		Rates:    service.NewRatesService(storages.Rates, components.Logger),
	}
}
