package workers

import (
	"context"
	"encoding/json"
	"fmt"
	"strconv"
	"time"

	"go.uber.org/zap"

	"gitlab.com/ptflp/go-rpc/internal/infrastructure/service"
	"gitlab.com/ptflp/go-rpc/internal/models"
	"gitlab.com/ptflp/go-rpc/internal/modules/rates/storage"
)

const (
	exchangeRatesInterval = 10 * time.Second
)

type ExchangeRates struct {
	exchangeService service.Exchenger
	repository      storage.Rater
}

func NewExchangeRates(exchangeService service.Exchenger, repository storage.Rater) *ExchangeRates {
	return &ExchangeRates{exchangeService: exchangeService, repository: repository}
}

func (e *ExchangeRates) Run(ctx context.Context, log *zap.Logger) {
	go func() {
		ticker := time.NewTicker(exchangeRatesInterval)

		for range ticker.C {
			log.Info("I'm worker and I am working...")
			//log.Printf("getting exchange rates...")
			rate := e.exchangeService.Ticker()
			rateDTO := models.RatesDTO{
				Ticker:    rate.Ticker,
				CreatedAt: rate.CreatedAt,
			}
			err := e.repository.AddTicker(ctx, rateDTO)
			if err != nil {
				log.Error("add ticker error", zap.Error(err))
				continue
			}
			data := make(map[string]models.Symbol)
			err = json.Unmarshal([]byte(rate.Ticker), &data)
			if err != nil {
				log.Error("unmarshal ticker error", zap.Error(err))
				continue
			}

			minRates := e.repository.GetMinPrices(ctx)
			if minRates == nil {
				log.Error("get min rates error", zap.Error(err))
				continue
			}
			minRatesMap := make(map[string]float64)
			for _, symbol := range minRates {
				minRatesMap[symbol.Symbol] = symbol.Price
			}

			maxRates := e.repository.GetMaxPrices(ctx)
			if maxRates == nil {
				log.Error("get max rates error", zap.Error(err))
				continue
			}
			maxRatesMap := make(map[string]float64)
			for _, symbol := range maxRates {
				maxRatesMap[symbol.Symbol] = symbol.Price
			}

			for symbol, value := range data {
				minTickerPrice, err := strconv.ParseFloat(value.Low, 64)
				if err != nil {
					log.Error("string to float64 conversion error", zap.Error(err))
				} else {
					minPrice, ok := minRatesMap[symbol]
					if !ok {
						err = e.repository.CreateMinPrice(ctx, symbol, minTickerPrice)
						if err != nil {
							log.Error(fmt.Sprintf("create %s min price error", symbol), zap.Error(err))
						}

					} else if minPrice > minTickerPrice {
						log.Info(fmt.Sprintf("Update %s min price: %f; min current price %f", symbol, minPrice, minTickerPrice))
						err = e.repository.UpdateMinPrice(ctx, symbol, minTickerPrice)
						if err != nil {
							log.Error(fmt.Sprintf("update %s min price error", symbol), zap.Error(err))
						}
					}
				}

				maxTickerPrice, err := strconv.ParseFloat(value.High, 64)
				if err != nil {
					log.Error("string to float64 conversion error", zap.Error(err))
				} else {
					maxPrice, ok := maxRatesMap[symbol]
					if !ok {
						err = e.repository.CreateMaxPrice(ctx, symbol, maxTickerPrice)
						if err != nil {
							log.Error(fmt.Sprintf("create %s max price error", symbol), zap.Error(err))
						}

					} else if maxPrice < maxTickerPrice {
						log.Info(fmt.Sprintf("Update %s max price: %f; max current price %f", symbol, maxPrice, maxTickerPrice))
						err = e.repository.UpdateMaxPrice(ctx, symbol, maxTickerPrice)
						if err != nil {
							log.Error(fmt.Sprintf("update %s max price error", symbol), zap.Error(err))
						}
					}
				}
			}

			select {
			case <-ctx.Done():
				return
			default:
			}
		}
	}()
}
