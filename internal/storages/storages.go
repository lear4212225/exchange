package storages

import (
	"gitlab.com/ptflp/go-rpc/internal/db/adapter"
	"gitlab.com/ptflp/go-rpc/internal/infrastructure/cache"
	"gitlab.com/ptflp/go-rpc/internal/modules/rates/storage"
)

type Storages struct {
	Rates storage.Rater
}

func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
	return &Storages{
		Rates: storage.NewRateStorage(sqlAdapter),
	}
}
