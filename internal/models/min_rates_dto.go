package models

import (
	"time"
)

//go:generate easytags $GOFILE json,db,db_ops,db_type,db_default,db_index

type MinRatesDTO struct {
	ID        int32     `json:"id" db:"id" db_type:"BIGSERIAL primary key" db_default:"not null"`
	Symbol    string    `json:"symbol" db:"symbol" db_ops:"create,update" db_type:"varchar(20)" db_default:"not null"`
	Price     float64   `json:"price" db:"price" db_ops:"create,update" db_type:"numeric" db_default:"not null"`
	UpdatedAt time.Time `json:"updated_at" db:"updated_at" db_ops:"create,update" db_type:"timestamp" db_default:"default (now()) not null"`
}

func (m *MinRatesDTO) TableName() string {
	return "min_rates"
}

func (m *MinRatesDTO) OnCreate() []string {
	return []string{}
}
