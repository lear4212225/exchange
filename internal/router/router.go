package router

import (
	"net/http"

	"github.com/go-chi/chi/v5"
	"gitlab.com/ptflp/go-rpc/internal/infrastructure/component"
	"gitlab.com/ptflp/go-rpc/internal/modules"
)

func NewApiRouter(controllers *modules.Controllers, components *component.Components) http.Handler {
	r := chi.NewRouter()

	r.Route("/api", func(r chi.Router) {
		r.Route("/1", func(r chi.Router) {
		})
	})

	return r
}
